const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const merchantSchema = new mongoose.Schema({

    fullName: {
        type: String,
        required: [true, 'fullName is required']
    },

    mobileNumber: {
        type: String,
        required: [true, 'mobileNumber is required']
	},

	location: {
		type: String,
		required: [true, 'location is required']
	},

    isProfileComplete: {
        type: Boolean
    },

    token: {
        type: String
    },

    fourDigitPin: {
        type: String,
        required: [true, 'fourDigitPin is required']
    },

    token: {
        type: String
    }
})

// create and store the token
merchantSchema.methods.generateToken = function(cb){
    var merch = this;
    var token = jwt.sign(merch._id.toHexString(),'supersecret')

    merch.token = token;
    merch.save(function(err, m){
        if (err) return cb(err)
        cb(null, m)
    })
}

merchantSchema.statics.findByToken = function(token, cb){
    const merch = this;
    jwt.verify(token,'supersecret',function(err, decode){
        merch.findOne({
            "_id":decode,
            "token":token
        }, function(err, m){
            if (err) return cb(err);
            cb(null, m)
        })
    })
}

const Merchant = mongoose.model('Merchant', merchantSchema)
module.exports = { Merchant }
