const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const thirdpartySchema = new mongoose.Schema({

    fullName: {
        type: String,
        required: [true, 'fullName is required']
    },

    mobileNumber: {
        type: Number,
        required: [true, 'mobileNumber is required']
    },

    fourDigitPin: {
        type: String,
        required: [true, 'fourDigitPin is required']
    }
})

// create and store the token
thirdpartySchema.methods.generateToken = function(cb){
    var third = this;
    var token = jwt.sign(third._id.toHexString(),'supersecret')

    third.token = token;
    third.save(function(err, m){
        if (err) return cb(err)
        cb(null, m)
    })
}

thirdpartySchema.statics.findByToken = function(token, cb){
    const third = this;
    jwt.verify(token,'supersecret',function(err, decode){
        third.findOne({
            "_id":decode,
            "token":token
        }, function(err, m){
            if (err) return cb(err);
            cb(null, m)
        })
    })
}

const ThirdParty = mongoose.model('thirdParty', thirdpartySchema)
module.exports = { ThirdParty }