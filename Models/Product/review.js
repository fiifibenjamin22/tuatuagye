const mongoose = require('mongoose');
const Product = require('./product');

let Schema = mongoose.Schema;

let ReviewSchema = new Schema({
    title: {
       type: String,
       required: true
    },
    text: String,
    product: {
        type: Schema.Types.ObjectId,
        ref: 'Product',
        required: true
    }
});

module.exports = mongoose.model('Review', ReviewSchema);