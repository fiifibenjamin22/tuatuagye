const mongoose = require('mongoose');
const Product = require('./product');
const Category = require('./category');

let Schema = mongoose.Schema;

let TypeSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    product: [{ type: Schema.Types.ObjectId, ref: 'Product' }],
    category: {
        type: Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    }
});

module.exports = mongoose.model('Type', TypeSchema);