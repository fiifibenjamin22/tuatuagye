const mongoose = require('mongoose');
const Review = require('./review');
const Category = require('./category');
const Type = require('./type');


let Schema = mongoose.Schema;

let ProductSchema = new Schema({
    image: {
        type: String,
        required: true
    },
    name: {
        type : String,
        required: true
    },
    make: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    specifications: {
        type: String,
        required: true
    },
    reviews: [{ type: Schema.Types.ObjectId, ref: 'Review' }],
    price: {
        type: String,
        required: true
    },
    type: {
        type: Schema.Types.ObjectId,
        ref: 'Type',
        required: true
    },
    is_active: {
        type: Boolean,
        default: true
    }
});

module.exports = mongoose.model('Product', ProductSchema);
