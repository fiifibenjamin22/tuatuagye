const mongoose = require('mongoose');
// const Type = require('./type');

let Schema = mongoose.Schema;

let CategorySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    type: [{ type: Schema.Types.ObjectId, ref: 'Type' }],
    is_active: {
        type: Boolean,
        default: true
    }
});

module.exports = mongoose.model('Category', CategorySchema);