const mongoose = require('mongoose');

const completePaymentSchema = mongoose.Schema({

	// payment info
	amountPaid: {
		type: String,
		required: ['amountPaid is required']
	},
	dateAndTime: {
		type: String,
		required: ['dateAndTime is required']
	},
	paymentMethod: {
		type: String,
		required: ['paymentMethod is required']
	},

	// product info
	productName: {
		type: String,
		required: ['productName is required']
	},
	productPrice: {
		type: String,
		required: ['productPrice is required']
	},
	productId: {
		type: String,
		required: ['productPrice is required']
	},

	// user info
	fullName: {
		type: String,
		required: [true, 'fullName is required']
	},
	mobileNumber: {
		type: String,
		required: [true, 'mobileNumber is required']
	},
	userId: {
		type: String,
		required: [true, 'userId is required']
	},
	userType: {
		type: String,
		required: ['userType is required']
	}
})

const finishedPayment = mongoose.model('finishedPayment', completePaymentSchema);
module.exports = { finishedPayment }
