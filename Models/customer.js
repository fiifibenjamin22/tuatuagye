const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const customerSchema = new mongoose.Schema({

    fullName: {
        type: String,
        required: [true, 'fullName is required']
    },

    mobileNumber: {
        type: String,
        required: [true, 'mobileNumber is required']
	},

	location: {
		type: String,
		required: [true, 'location is required']
	},

    fourDigitPin: {
        type: String,
        required: [true, 'fourDigitPin is required']
    },

    type: {
        type: String
    },

    isProfileComplete: {
        type: Boolean
    },

    token: {
        type: String
    }
})

customerSchema.methods.comparePassword = function(customerPassword, cb){
    bcrypt.compare(customerPassword , this.fourDigitPin, function(err, isMatch) {
        if (err) {
            return cb(err)
        }else{
            cb(null, isMatch)
        }
    })
}

// create and store the token
customerSchema.methods.generateToken = function(cb){
    var cust = this;
    var token = jwt.sign(cust._id.toHexString(),'supersecret')

    cust.token = token;
    cust.save(function(err, custom){
        if (err) return cb(err)
        cb(null, custom)
    })
}

customerSchema.statics.findByToken = function(token, cb){
    const cust = this;
    jwt.verify(token,'supersecret',function(err, decode){
        cust.findOne({
            "_id":decode,
            "token":token
        }, function(err, cus){
            if (err) return cb(err);
            cb(null, cus)
        })
    })
}

const Customer = mongoose.model('Customer', customerSchema)
module.exports = { Customer }
