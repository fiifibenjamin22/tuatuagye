const { Customer } = require('./../Models/customer')

let customer_auth = (req, res, next) => {
    let token = req.cookies.auth;

    Customer.findByToken(token, (err, cust) => {
        if (err){
            res.status(400).json({
                message: err
            })
        }else{
            req.token = cust
            next()
        }
    })
}

module.exports = { customer_auth }