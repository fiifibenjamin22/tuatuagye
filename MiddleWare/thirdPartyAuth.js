const { ThirdParty } = require('./../Models/thirdparty')

let thirdParty_auth = (req, res, next) => {
    let token = req.cookies.auth;

    ThirdParty.findByToken(token, (err, cust) => {
        if (err){
            res.status(400).json({
                message: err
            })
        }else{
            req.token = cust
            next()
        }
    })
}


module.exports = { thirdParty_auth }