const { Merchant } = require('./../Models/merchant')

let merchant_auth = (req, res, next) => {
    let token = req.cookies.auth;

    Merchant.findByToken(token, (err, cust) => {
        if (err){
            res.status(400).json({
                message: err
            })
        }else{
            req.token = cust
            next()
        }
    })
}

module.exports = { merchant_auth }