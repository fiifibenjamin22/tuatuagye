const express = require('express')
const body_parser = require('body-parser')
const cors = require('cors')
const mongoose = require('mongoose')
const mongodbUri = require('mongodb-uri')
const cookie_parser = require('cookie-parser')
const morgan = require('morgan')
const app = express()

// import routes
const customerRegistration = require('./Routes/Authentication/customer')
const merchantRegistration = require('./Routes/Authentication/merchant')
const thirdPartyRegistration = require('./Routes/Authentication/thirdParty')
const Authentication = require('./Routes/Authentication/Authentication')
const products = require('./Routes/Product/product')
const review = require('./Routes/Product/review')
const category = require('./Routes/Product/category')
const type = require('./Routes/Product/type')

const ussdProducts = require('./Routes/Product/ussd.product')

// allow cross origin on a different port
app.use(cors({
    origin: 'http://localhost:3000'
}))

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(body_parser.json())
app.use(body_parser.urlencoded({
    extended: false
}))
app.use(cookie_parser())
app.use(morgan('dev'))

// || 'mongodb://localhost:27017/tuatuagye'
// 'mongodb://tuatuagye:brich2018@ds159129.mlab.com:59129/tuatuagye-db' || 

// connect to mongodb
mongoose.Promise = global.Promise
var uri = 'mongodb://tuatuagye:brich2018@ds159129.mlab.com:59129/tuatuagye-db';

var mongooseConnectString = mongodbUri.formatMongoose(uri);
mongoose.connect(mongooseConnectString,{
    useNewUrlParser: true
 });

// Test for connection success
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error: '));
db.once('open', function callback () {
    console.log('Successfully connected to MongoDB');
});

// connect routes
app.use('/api/v1/ttg', customerRegistration)
app.use('/api/v1/ttg', merchantRegistration)
app.use('/api/v1/ttg', thirdPartyRegistration)
app.use('/api/v1/ttg/ussd/', Authentication)
app.use('/api/v1/ttg/ussd/', ussdProducts)
app.use('/api/v1/ttg/product', products)
app.use('/api/v1/ttg/review', review)
app.use('/api/v1/ttg/category', category)
app.use('/api/v1/ttg/type', type)

//error handling
app.use((err, req, res, next) => {
    res.status(400).send({
        error: err.message
    })
})

// start server and listen on port
var server = app.listen(process.env.PORT || 3200, () => {
    var port = server.address().port;
    console.log('App is now running on port', port)
});