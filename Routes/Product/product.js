const express = require('express');
const Product = require('./../../Models/Product/product');
const Type = require('./../../Models/Product/type');
const router = express.Router();

/**
 * CRUD operations: Create Read Update Delete
 */
// Add product for a specific type
// '/v1/ttg/product/add/:id' - Create
router.post('/add/:id', (req, res) => {
    Type.findById(req.params.id, (err, type) => {
        if (err) {
            res.send(err);
        }

        let product = new Product();
        product.image = req.body.image;
        product.name = req.body.name;
        product.make = req.body.make;
        product.description = req.body.description;
        product.specifications = req.body.specifications;
        product.price = req.body.price;

        //problem 1. ::todo
        product.type = type._id;

        product.save((err, prod) => {
            if (err) {
                res.status(400).json({
                    message: err
                })
            }
            type.product.push(prod);
            type.save((err, result) => {
                if (err) {
                    res.status(400).json({
                        message: err
                    })
                }else{
                    res.status(200).json({
                        message: result
                    })
                }
            })
        });
    });
});

// '/v1/ttg/product' - Read
router.get('/', (req, res) => {
    Product.find({}, (err, products) => {
        if (err) {
            res.send(err);
        }
        res.json(products);
    });
});

// '/v1/ttg/product/:id' - Read: Get product by id
router.get('/:id', (req, res) => {
    Product.findById(req.params.id, (err, product) => {
        if (err) {
            res.send(err);
        }
        res.json(product);
    });
});

// '/v1/ttg/product/:name' - Read: Get product by name
router.get('/name/:name', (req, res) => {
    Product.find({ name:req.params.name }, function (err, product) {
        if (err) {
            res.send(err);
        }
        res.json(product);
    });
});

// '/v1/ttg/product/:make' - Read: Get product by make
router.get('/make/:make', (req, res) => {
    Product.find({ make:req.params.make }, function (err, product) {
        if (err) {
            res.send(err);
        }
        res.json(product);
    });
});

// '/v1/ttg/product/:price' - Read: Get product by price
router.get('/price/:price', (req, res) => {
    Product.find({ price:req.params.price }, function (err, product) {
        if (err) {
            res.send(err);
        }
        res.json(product);
    });
});

// '/v1/ttg/product/:type' - Read: Get product by type
router.get('/type/:type', (req, res) => {
    Product.find({ type:req.params.type }, function (err, product) {
        if (err) {
            res.send(err);
        }
        res.json(product);
    });
});

// '/v1/ttg/product/:id' - Update
// router.put('/:id', (req, res) => {
//     Product.findById(req.params.id, (err, product) => {
//         if (err) {
//             res.send(err);
//         }

//         product.image = req.body.image;
//         product.name = req.body.name;
//         product.make = req.body.make;
//         product.description = req.body.description;
//         product.specifications = req.body.specifications;
//         product.price = req.body.price;
//         product.type = product._id;

//         product.save((err, result) => {
//             if (err) {
//                 res.status(400).json({
//                     message: err
//                 });
//             }else{
//                 res.status(200).json({
//                     message: result
//                 })
//             }
//         });
//     });
// });

router.put('/:id',function (req,res,next) {
    Product.findByIdAndUpdate({
        _id : req.params.id
    }, req.body).then(function () {
        Product.findOne({
            _id : req.params.id
        }).then(function (err, result) {
            if (err) {
                res.json({
                    message: err
                });
            }else{
                res.json({
                    message: result
                })
            }
        });
    });
});

// '/v1/ttg/product/:id' - Delete
router.delete('/:id', (req, res) => {
    Product.remove({
        _id: req.params.id
    }, (err, product) => {
        if (err) {
            res.send(err);
        }
        res.json({ message: 'Product Details Deleted Successfully!' });
    });
});

module.exports = router;
