const express = require('express')
const { MainCustomer } = require('./../../Models/customerMainDb')
const mainMerchDB = require('./../../Models/merchantMainDb')
const productDB = require('./../../Models/Product/product')
const categoryDB = require('./../../Models/Product/category')
const typeDB = require('./../../Models/Product/type')
const bcrypt = require('bcryptjs')
const router = express.Router();

// show ussd Categories
router.post('/categories', (req, res) => {

    categoryDB.find({

    }, (err, categs) => {
        if (err) {
            res.status(200).json({
                message: err
            })
        }else{
            res.status(200).send(`Welcome to TuTua Gye Select a category to continue to continue:
            1. ${categs[0].name}
            0. Exit`)
        }
    })
})

// show types under each category
router.post('/types/catSelected', (req, res) => {
    let category = '';
    let type = '';
    let input = req.body.catSelected;

    if (input == 1) {
        //1
        category = 'Home Appliances';
        // type = 'Rice Cooker'
        categoryDB.find({

        }, (err, result) => {
            if (err){
                res.status(400).json({
                    message: err
                })
            }else{
                typeDB.findById(result[0].type[0], (err, typesUnderCategory) => {
                    if (err) {
                        res.status(400).json({
                            message: err
                        })
                    }else{
                        res.status(200).send(`Please Select a type to continue:
                        1. ${typesUnderCategory.name}
                        0. Exit`)
                    }
                });
            }
        })
    }else if (input == 2) {
        //2
        category = 'Brich Products';
        type = 'Laptops';
        // fetch all categories
        categoryDB.find({

        }, (err, result) => {
            if (err){
                res.status(400).json({
                    message: err
                })
            }else{
                let it = result[0].type;
                let i = '';
                it.forEach(element => {
                    // console.log(element)
                    i = element;
                });

                typeDB.findById(i, (err, typesUnderCategory) => {
                    console.log(i);
                    if (err) {
                        res.status(400).json({
                            message: err
                        })
                    }else{
                        res.status(200).send(`Please Select a type to continue:
                        1. ${typesUnderCategory.name}
                        0. Exit`)
                    }
                });
            }
        })
    }else if (input == 3) {
        category = 'Others'
        categoryDB.find({

        }, (err, result) => {
            if (err){
                res.status(400).json({
                    message: err
                })
            }else{
                typeDB.findById(result[2].type[1], (err, typesUnderCategory) => {
                    if (err) {
                        res.status(400).json({
                            message: err
                        })
                    }else{
                        res.status(200).send(`Please Select a type to continue:
                        1. ${typesUnderCategory.name}
                        0. Exit`)
                    }
                });
            }
        });
    }else {
        return
    }
})

// show products under types
router.post('/products', (req, res, next) => {
    let type = req.body.type;
    let inputSelected = req.body.inputSelected;

    if (inputSelected == 1) {

		typeDB.findOne({
			'name': type
		}, (err, typeFound) => {
			if (err){
				return next(err)
			}else{
				// get typeId and type Name
				console.log(typeFound.product[0]);
				console.log(typeFound.name);

				if (type == typeFound.name) {

					// get all products under this type
					productDB.find({
						"_id": typeFound.product[0]
					}, (err, product) => {
						if (err) {
							return next(err)
						}else{
							console.log(product);

							res.status(200).send(`Please Select a Product to Start Payment:
							1. ${product[0].name}
							0. Exit`)
						}
					})
				}
			}
		})

    }else if (inputSelected == 2) {

		typeDB.find({
			'name' : type
		}, (err, typeFound) => {
			if (err) {
				return next(err)
			}else{
				// get typeId and type Name
				console.log(typeFound.product[1]);
				console.log(typeFound.name);

				if (type == typeFound.name) {
					productDB.find({
						'_id': typeFound.product[1]
					}, (err, product) => {
						if (err){
							return next(err);
						}else{
							console.log(product);
							res.status(200).send(`Please Select a Product to Start Payment:
							1. ${product[2].name}
							0. Exit`)
						}
					})
				}
			}
		})
    }else if (inputSelected == 3) {
		typeDB.find({
			'name': type
		}, (err, typeFound) => {
			if (err) {
				return next(err);
			}else{
				if (type == typeFound.name) {
					// get typeId and type Name
					console.log(typeFound.product[2]);
					console.log(typeFound.name);

					productDB.find({
						'_id': typeFound.product[3]
					}, (err, product) => {
						if (err){
							return next(err);
						}else{
							console.log(product);
							res.status(200).send(`Please Select a Product to Start Payment:
							1. ${product[3].name}
							0. Exit`)
						}
					})
				}
			}
		})
    }
})

// Select a product
router.post('/selectedproduct', (req, res) => {
	var inputSelected = req.body.inputSelected;
	var productName = req.body.productName;

	if (inputSelected == 1) {
		productDB.findOne({
			name: productName
		}, (err, product) => {
			if (err){
				res.status(400).json({
					message: err
				})
			}else{
				res.status(200).json({
					message: product
				})
			}
		})
	}
})

// start payment
router.post('/pay', (req, res, next) => {
	var amount = req.body.amountPaid;
	var pin = req.body.pin;
	var productId = req.body.productId;
	var mobileNumber = req.body.mobileNumber;

	productDB.findOne({
		'_id': productId
	}, (err, product) => {
		if (err) {
			res.status(400).json({
				message: err
			})
		}else{
			// product info is availiable for payment to start
			const name = product.name
			console.log(name);

			// get user by user phoneNumber
			MainCustomer.findOne({
				'mobileNumber' : mobileNumber
			}, (err, theCustomer) => {
				if (err){
					return next(err)
				}else{
					console.log(theCustomer.fourDigitPin);
					bcrypt.compare(pin, theCustomer.fourDigitPin, (err, isMatch) => {
						if  (err){
							return next(err)
						}else{
							if (isMatch === false){
								res.status(400).json({
									message: 'Password is not correct, please type again'
								})
							}else{

								// compare product price with amount to be paid
								const productPrice = product.price;

								if (productPrice == amount){
									res.send(productPrice)
								}else{
									res.send('not working');
								}

								// res.status(200).json({
								// 	payment_info: {
								// 		amountPaid: amount,
								// 		product: product,
								// 		customer: theCustomer
								// 	}
								// })
							}
						}
					})
				}
			})
		}
	})
})

module.exports = router;
