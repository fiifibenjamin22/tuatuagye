const express = require('express');
const Category = require('./../../Models/Product/category');
const Type = require('./../../Models/Product/type');
const router = express.Router();

// add review for a specific product id
// '/v1/ttg/type/add/:id' - Create
router.post('/add/:id', (req, res) => {
    Category.findById(req.params.id, (err, category) => {
        if (err) {
            res.send(err);
        }
        let type = new Type();
        type.name = req.body.name;
        type.category = category._id;

        type.save((err, type) => {
            if (err) {
                res.send(err);
            }
            category.type.push(type);
            category.save((err, result) => {
                if (err) {
                    res.send(err);
                }
                res.json({ message: result })
            });
        });

    });
});

// '/v1/ttg/type' - Read
router.get('/', (req, res) => {
    Type.find({}, (err, types) => {
        if (err) {
            res.send(err);
        }
        res.json(types);
    });
});

// '/v1/ttg/type/:id' - Read
router.get('/:id', (req, res) => {
    Type.findById(req.params.id, (err, type) => {
        if (err) {
            res.send(err);
        }
        res.json(type);
    });
});

// '/v1/ttg/type/name/:name' - Read
router.get('/name/:name', (req, res) => {
    Type.find({ name:req.params.name }, function(err, type) {
        if (err) {
            res.send(err);
        }
        res.json(type);
    });
});

// '/v1/ttg/type/:id' - Update
router.put('/:id', (req, res) => {
    Type.findById(req.params.id, (err, type) => {
        if (err) {
            res.send(err);
        }
        type.name = req.body.name;
        //type.category = req.body.category._id;

        type.save(err => {
            if (err) {
                res.send(err);
            }
            res.json({ message: 'Type Details Updated Successfully!'});
        });
    });
});

// '/v1/ttg/type/:id' - Delete
router.delete('/:id', (req, res) => {
    Type.remove({
        _id: req.params.id
    }, (err, type) => {
        if (err) {
            res.send(err);
        }
        res.json({ message: 'Type Details Deleted Successfully!' });
    });
});

module.exports = router;

