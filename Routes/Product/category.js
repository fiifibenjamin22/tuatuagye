const express = require('express');
const Category = require('./../../Models/Product/category');
const router = express.Router();

/**
 * CRUD operations: Create Read Update Delete
 */
// '/v1/ttg/category/add' - Create
router.post('/add', (req, res) => {
    let category = new Category();
    category.name = req.body.name;

    category.save((err, result) => {
        if (err) {
            res.status(400).json({
                error: err.message
            })
        }else{
            res.status(200).json({
                message: result
            })
        }
    })
});

// '/v1/ttg/category' - Read
router.get('/', (req, res) => {
    Category.find({}, (err, categories) => {
        if (err) {
            res.send(err);
        }
        res.json(categories);
    });
});

// '/v1/ttg/category/:id' - Read: Get category by id
router.get('/:id', (req, res) => {
    Category.findById(req.params.id, (err, category) => {
        if (err) {
            res.send(err);
        }
        res.json(category);
    });
});

// '/v1/ttg/category/:name' - Read: Get category by name
router.get('/name/:name', (req, res) => {
    Category.find({ name:req.params.name }, function (err, category) {
        if (err) {
            res.send(err);
        }
        res.json(category);
    });
});

// '/v1/ttg/category/:id' - Update
router.put('/:id', (req, res) => {
    Category.findById(req.params.id, (err, category) => {
        if (err) {
            res.send(err);
        }
        category.name = req.body.name;
        category.save(err => {
            if (err) {
                res.send(err);
            }
            res.json({ message: 'Category Details Updated Successfully!' });
        });
    });
});

// '/v1/ttg/category/:id - Delete
router.delete('/:id', (req, res) => {
    Category.remove({
        _id: req.params.id
    }, (err, category) => {
        if (err) {
            res.send(err);
        }
        res.json({ message: 'Category Details Deleted Successfully!' });
    });
});

module.exports = router;