const express = require('express');
const Product = require('./../../Models/Product/product');
const Review = require('./../../Models/Product/review');
const router = express.Router();

// add review for a specific product id
// '/v1/ttg/review/add/:id' - Create
router.post('/add/:id', (req, res) => {
    Product.findById(req.params.id, (err, product) => {
        if (err) {
            res.send(err);
        }
        let review = new Review();
        review.title = req.body.title;
        review.text = req.body.text;
        review.product = product._id;
        review.save((err, review) => {
            if (err) {
                res.send(err);
            }
            product.reviews.push(review);
            product.save(err => {
                if (err) {
                    res.send(err);
                }
                res.json({ message: 'Product Review Added Successfully!' })
            });
        });
    });
});

// '/v1/ttg/review' - Read
router.get('/', function(req, res){
    Review.find({}, function(err, reviews){
        if (err) {
            res.send(err);
        }
        res.json(reviews);
    });
});

// '/v1/ttg/review/:id' - Read
router.get('/:id', function (req, res) {
    Review.findById(req.params.id, function (err, review) {
        if (err) {
            res.send(err);
        }
        res.send(review);
    });
});

// '/v1/ttg/review/:id' - Update
router.put('/:id', function (req, res) {
    Review.findById(req.params.id, function (err, review) {
        if (err) {
            res.send(err);
        }
        review.title = req.body.title;
        review.text = req.body.text;
        review.product = product._id;

        review.save(function (err){
            if (err) {
                res.send(err);
            }
            res.json({ message: "Review Details Edited Successfully" });
        });
    });
});

// '/v1/ttg/review/:id' - Delete
router.delete('/:id', function (req, res) {
    Review.remove({
        _id: req.params.id
    }), function (err, review) {
        if (err) {
            res.send(err);
        }
        res.json({ message: "Review Details Deleted Successfully" });
    }
});

module.exports = router;