const express = require('express')
const bcrypt = require('bcryptjs')
const { thirdParty } = require('./../../Models/thirdparty')
const { thirdParty_auth } = require('./../../MiddleWare/thirdPartyAuth')
const router = express.Router()
const SALT_I = 10

router.post('/user/register', (req, res, next) => {

    const fourDigit = req.body.fourDigitPin

    bcrypt.genSalt(SALT_I, function(err, salt){
        if (err){
            return next(err)
        }else{
            bcrypt.hash(fourDigit, salt, function(err, hash){
                if (err){
                    return next(err)
                }else{
                    
                    const customer = new thirdParty({
                        fullName: req.body.fullName,
                        mobileNumber: req.body.mobileNumber,
                        fourDigitPin: hash
                    })

                    customer.save((err, tuatuagyeCustomer) => {
                        if (err){
                            res.status(400).json({
                                message: `some error: ${err}`
                            })
                        }else{
                            res.status(200).json({
                                message: tuatuagyeCustomer
                            })
                        }
                    })
                }
            })
        }
    })
})

// sign-in
router.post('/user/signin', (req, res, next) => {
    thirdParty.findOne({
        'mobileNumber': req.body.mobileNumber
    }, (err, theCustomer) => {
        if (err){
            return next(err)
        }else{
            theCustomer.comparePassword(req.body.fourDigitPin, (err, isMatch) => {
                if (err) {
                    return next(err)
                }else{
                    if (isMatch === false){
                        res.status(400).json({
                            message: 'Password is not correct, please type again'
                        })
                    }else{

                        theCustomer.generateToken((err, custom) => {
                            if (err){
                                res.status(400).json({
                                    message: err
                                })
                            }else{
                                res.cookie('auth', custom.token).json({
                                    message: theCustomer
                                })
                            }
                        })
                    }
                }
            })
        }
    })
})

// test authentication
router.get('/user/profile', thirdParty_auth,(req, res) => {
    res.status(200).json({
        message: req.token
    })
})

module.exports = router