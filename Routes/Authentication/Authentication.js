const express = require('express')
const merchantModel = require('./../../Models/merchant')
const customerModel = require('./../../Models/customer')
const mainCustDb = require('./../../Models/customerMainDb')
const mainMerchDB = require('./../../Models/merchantMainDb')
const router = express.Router()
const SALT_I = 10
const bcrypt = require('bcryptjs')

// 1 - first Menu
router.post('/start', (req, res, next) => {
    res.status(200).send(`Welcome to TuTua Gye Select your option to continue:
    1. Customer
    2. Merchant`)
})

// 2 - second Menu
router.post('/optionSelected', (req, res) => {
    let type = '';
    let input = req.body.optionSelected;
    if (input == 1){
        type = 'customer'
        res.status(200).json({
            type,
            message: "Please enter your Mobile Money Number"
        })
    }else if (input == 2){
        type = 'merchant'
        res.status(200).json({
            type,
            message: "Please enter your Mobile Money Number"
        })
    }
})

// 3 - phone number
router.post('/phoneNumber', (req, res) => {

    var merchantPhoneNumber;
    var customerPhoneNumber;
    let type = req.body.type;

    if (type == 'customer'){
        customerPhoneNumber = req.body.customerPhoneNumber
        // save into DB
        checkUserExistence()
        function checkUserExistence(){
            customerModel.Customer.findOne({
                // _id: _id,
                'mobileNumber': customerPhoneNumber
            }, (err, custom) => {
                if (err){
                    return res.status(400).json({
                        message: err
                    })
                }else if (custom == null){

                    // if customer does not exist create customer and save into Dummy DB.
                    const customerName = new customerModel.Customer({
                        fullName: '_',
						mobileNumber: customerPhoneNumber,
						location: '-',
                        fourDigitPin: '_',
                        isProfileComplete: false,
                        type: 'customer'
                    })

                    customerName.save((err, tuatuagyeCustomer) => {
                        if (err){
                            res.status(400).json({
                                message: `some error: ${err}`
                            })
                        }else{
                            res.status(200).json({
                                tuatuagyeCustomer
                            })
                        }
                    })

                }else{

                    // if customer already exist redirect to Products Here.
                    return res.status(200).json({
                        message: "customer already exist."
                    })
                }
            })
        }

        // if User is a Merchant
    }else if (type == 'merchant'){
        merchantPhoneNumber = req.body.merchantPhoneNumber
        // save into DB
        checkMerchantExistence()
        function checkMerchantExistence(){
            merchantModel.Merchant.findOne({
                // _id: _id,
                'mobileNumber': merchantPhoneNumber
            }, (err, merch) => {
                if (err){
                    return res.status(400).json({
                        message: err
                    })
                }else if (merch == null){

                    // if Merchant does not exist create customer and save into Dummy DB.
                    const merchantName = new merchantModel.Merchant({
                        fullName: '_',
                        mobileNumber: merchantPhoneNumber,
						location: '_',
						fourDigitPin: '_',
                        isProfileComplete: false,
                        type: 'Merchant'
                    })

                    merchantName.save((err, tuatuagyeMerchant) => {
                        if (err){
                            res.status(400).json({
                                message: `some error: ${err}`
                            })
                        }else{
                            res.status(200).json({
                                tuatuagyeMerchant
                            })
                        }
                    })

                }else{

                    // if customer already exist
                    return res.status(301).json({
                        message: "merchant Already Exist."
                    })
                }
            })
        }
    }
})

// 4 - update with fullname
router.put('/fullName', (req, res) => {
    var customerFullName;
    var merchantFullName;
    let type = req.body.type;
    let ids = req.body._id

    if (type == 'customer'){
        customerFullName = req.body.fullName
        // save into DB
        findAnbdSaveName()
        function findAnbdSaveName(){
            customerModel.Customer.findByIdAndUpdate(ids,{
                fullName: customerFullName,
            }, {
                new: true
            }).then(tuatuagyeCustomer => {
                if (!tuatuagyeCustomer){
                    return res.status(404).send({
                        message: "Not found with id"
                    });
                }
                res.send(tuatuagyeCustomer)
            })
        }

    }else if (type == 'merchant'){
        merchantFullName = req.body.fullName
        // save into DB
        findAnbdSaveName()
        function findAnbdSaveName(){
            merchantModel.Merchant.findByIdAndUpdate(ids,{
                fullName: merchantFullName,
            }, {
                new: true
            }).then(tuatuagyeMerchant => {
                if (!tuatuagyeMerchant){
                    return res.status(404).send({
                        message: "Not found with id"
                    });
                }
                res.send(tuatuagyeMerchant)
            })
        }
    }
})

// 4b - location
router.put('/location', (req, res) => {
	var customerLocation;
	var merchantLocation;
	var type = req.body.type;
	var ids = req.body._id;

	if (type == 'customer') {
		customerLocation = req.body.customerLocation;
		findAndSavelocation()
		// find customer by id and save location
		function findAndSavelocation() {
			customerModel.Customer.findByIdAndUpdate(ids, {
				location: customerLocation
			}, {
				new: true
			}).then(tuatuagyeCustomer => {
				if (!tuatuagyeCustomer) {
					return res.status(404).send({
						message: "user not found"
					})
				}
				res.send(tuatuagyeCustomer);
			})
		}
	}else if (type == 'merchant') {
		merchantLocation = req.body.merchantLocation;
		findAndSavelocation()
		// find customer by id and save location
		function findAndSavelocation() {
			merchantModel.Merchant.findByIdAndUpdate(ids, {
				location: merchantLocation
			}, {
				new: true
			}).then(tuatuagyeMerchant => {
				if (!tuatuagyeMerchant) {
					return res.status(404).send({
						message: "user not found"
					})
				}
				res.send(tuatuagyeMerchant);
			})
		}
	}
})

//update with four-digit-pin
router.put('/pin', (req, res, next) => {

    var customerPin;
    var merchantPin;
    let type = req.body.type;
    let ids = req.body._id;

    if (type == 'customer'){

		customerPin = req.body.customerPin
		// hash customer pin
		bcrypt.genSalt(SALT_I, (err, salt) => {
			if (err) {
				return next(err)
			}else{
				bcrypt.hash(customerPin, salt, (err, hash) => {
					if (err){
						return next(err)
					}else{
						customerModel.Customer.findByIdAndUpdate(ids,{
							fourDigitPin: hash,
						}, {
							new: true
						}).then(tuatuagyeCustomer => {
							if (!tuatuagyeCustomer){
								return res.status(404).send({
									message: "Not found with id"
								});
							}

							if (tuatuagyeCustomer.mobileNumber !== null && tuatuagyeCustomer.fullName !== null && tuatuagyeCustomer.location !== null && tuatuagyeCustomer.fourDigitPin !== null){
								customerModel.Customer.findByIdAndUpdate(ids,{
									isProfileComplete: true
								}, {
									new: true
								}).then(tuatuagyeMainCustomerDb => {
									const customer = new mainCustDb.MainCustomer({
										fullName: tuatuagyeMainCustomerDb.fullName,
										mobileNumber: tuatuagyeMainCustomerDb.mobileNumber,
										location: tuatuagyeMainCustomerDb.location,
										fourDigitPin: tuatuagyeMainCustomerDb.fourDigitPin,
										isProfileComplete: tuatuagyeMainCustomerDb.isProfileComplete,
										type: tuatuagyeMainCustomerDb.type
									})

									customer.save((err, tuatuagyeMainCust) => {
										if (err){
											res.status(400).json({
												message: `some error: ${err}`
											})
										}else{

											// save into the new DB
											res.status(200).json({
												tuatuagyeMainCust
											})
										}
									})
								})
							}
						})
					}
				})
			}
		})

    }else if (type == 'merchant'){

		merchantPin = req.body.merchantPin

		bcrypt.genSalt(SALT_I, (err, salt) => {
			if (err) {
				return next(err)
			}else{
				bcrypt.hash(merchantPin, salt, (err, hash) => {
					if (err){
						return next(err)
					}else{
						merchantModel.Merchant.findByIdAndUpdate(ids,{
							fourDigitPin: hash,
						}, {
							new: true
						}).then(tuatuagyeMerchant => {
							if (!tuatuagyeMerchant){
								return res.status(404).send({
									message: "Not found with id"
								});
							}

							if (tuatuagyeMerchant.mobileNumber !== null && tuatuagyeMerchant.fullName !== null && tuatuagyeMerchant.location !== null && tuatuagyeMerchant.fourDigitPin !== null){
								merchantModel.Merchant.findByIdAndUpdate(ids,{
									isProfileComplete: true
								}, {
									new: true
								}).then(tuatuagyeMainMerchantDb => {
									const merchant = new mainMerchDB.MainMerchant({
										fullName: tuatuagyeMainMerchantDb.fullName,
										mobileNumber: tuatuagyeMainMerchantDb.mobileNumber,
										location: tuatuagyeMainMerchantDb.location,
										fourDigitPin: tuatuagyeMainMerchantDb.fourDigitPin,
										isProfileComplete: tuatuagyeMainMerchantDb.isProfileComplete,
										type: tuatuagyeMainMerchantDb.type
									})

									merchant.save((err, tuatuagyeMainMerch) => {
										if (err){
											res.status(400).json({
												message: `some error: ${err}`
											})
										}else{

											// save into the new DB
											res.status(200).json({
												tuatuagyeMainMerch
											})
										}
									})
								})
							}
						})
					}
				})
			}
		})
    }
})

module.exports = router
