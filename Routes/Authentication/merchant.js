const express = require('express')
const bcrypt = require('bcryptjs')
const { Merchant } = require('./../../Models/merchant')
const { merchant_auth } = require('./../../MiddleWare/merchantAuth')
const router = express.Router()
const SALT_I = 10

router.post('/merchant/register', (req, res, next) => {

    const fourDigit = req.body.fourDigitPin

    bcrypt.genSalt(SALT_I, function(err, salt){
        if (err){
            return next(err)
        }else{
            bcrypt.hash(fourDigit, salt, function(err, hash){
                if (err){
                    return next(err)
                }else{

                    const merchant = new Merchant({
                        fullName: req.body.fullName,
                        mobileNumber: req.body.mobileNumber,
                        fourDigitPin: hash
                    })

                    Merchant.findOne({
                        mobileNumber: req.body.mobileNumber
                    }, (err, theMerchant) => {
                        if (err){
                            return res.status(400).json({
                                message: err
                            })
                        }else{
                            if (theMerchant){
                                return res.status(409).json({
                                    message: 'You are already registered as a merchant'
                                })
                            }else{
                                merchant.save((err, tuatuagyeMerchant) => {
                                    if (err){
                                        res.status(400).json({
                                            message: `some error: ${err}`
                                        })
                                    }else{
                                        res.status(200).json({
                                            message: tuatuagyeMerchant
                                        })
                                    }
                                })
                            }
                        }
                    })
                }
            })
        }
    })
})

// sign-in
router.post('/merchant/signin', (req, res, next) => {
    Merchant.findOne({
        mobileNumber: req.body.mobileNumber
    }, (err, theMerchant) => {
        if (err){
            return next(err)
        }else{

            theMerchant.comparePassword(req.body.fourDigitPin, (err, isMatch) => {
                if (err) {
                    return next(err)
                }else{
                    if (isMatch === false){
                        res.status(400).json({
                            message: 'Password is not correct, please type again'
                        })
                    }else{

                        theMerchant.generateToken((err, merch) => {
                            if (err){
                                res.status(400).json({
                                    message: err
                                })
                            }else{
                                res.cookie('auth', merch.token).json({
                                    message: theMerchant
                                })
                            }
                        })
                    }
                }
            })
        }
    })
})

// test authentication
router.get('/merchant/profile', merchant_auth,(req, res) => {
    res.status(200).json({
        message: req.token
    })
})

module.exports = router
