const express = require('express')
const bcrypt = require('bcryptjs')

const { MainCustomer } = require('./../../Models/customerMainDb')
const { customer_auth } = require('./../../MiddleWare/customerAuth')
const router = express.Router()
const SALT_I = 10

router.post('/customer/register', (req, res, next) => {

	const fourDigit = req.body.fourDigitPin
	const mobileNumber = req.body.mobileNumber

	MainCustomer.findOne({
		'mobileNumber': mobileNumber
	}, (err, user) => {
		if (err) {
			res.status(400).json({
				message: err
			})
		}else {
			if (user) {
				res.status(301).json({
					message: 'You have already registered'
				})
			}else{

				bcrypt.genSalt(SALT_I, function(err, salt){
					if (err){
						return next(err)
					}else{
						bcrypt.hash(fourDigit, salt, function(err, hash){
							if (err){
								return next(err)
							}else{

								const customer = new MainCustomer({
									fullName: req.body.fullName,
									mobileNumber: mobileNumber,
									location: req.body.location,
									fourDigitPin: hash
								})

								customer.save((err, tuatuagyeCustomer) => {
									if (err){
										res.status(400).json({
											message: `some error: ${err}`
										})
									}else{
										res.status(200).json({
											message: tuatuagyeCustomer
										})
									}
								})
							}
						})
					}
				})
			}
		}
	})
})

// sign-in
router.post('/customer/signin', (req, res, next) => {
    MainCustomer.findOne({
        'mobileNumber': req.body.mobileNumber
    }, (err, theCustomer) => {
        if (err){
            return next(err)
        }else{
            theCustomer.comparePassword(req.body.fourDigitPin, (err, isMatch) => {
                if (err) {
                    return next(err)
                }else{
                    if (isMatch === false){
                        res.status(400).json({
                            message: 'Password is not correct, please type again'
                        })
                    }else{

                        theCustomer.generateToken((err, custom) => {
                            if (err){
                                res.status(400).json({
                                    message: err
                                })
                            }else{
                                res.cookie('auth', custom.token).json({
                                    message: theCustomer
                                })
                            }
                        })
                    }
                }
            })
        }
    })
})

// test authentication
router.get('/customer/profile', customer_auth,(req, res) => {
    res.status(200).json({
        message: req.token
    })
})

// test get all users
router.get('/customers/all', (req, res) => {
	MainCustomer.find({

	}, (err, allCustomers) => {
		if (err) throw err;
		res.status(200).json({
			allCustomers
		})
	})
})

module.exports = router
